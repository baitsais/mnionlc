import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/*
  Generated class for the Edit page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

export class EditPage {

constructor( public viewCtrl: ViewController, private _navParams: NavParams) {
    console.log("initialize NewItemModal")
    console.log(this._navParams.get("userid"))
}

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
