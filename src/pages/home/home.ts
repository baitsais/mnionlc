import { ProfilePage } from './../profile/profile';
import { VideoPage } from './../video/video';
import { SearchPage } from './../search/search';
import { Mediaservice } from './../../providers/mediaservice';
import { Loginservice } from './../../providers/loginservice';
import { LoginPage } from './../login/login';
import { UploadPage } from './../upload/upload';
import { Component, OnInit } from '@angular/core';

//import { bootstrap, provide} from 'angular2/angular2';

import { NavController, MenuController, LoadingController, NavParams } from 'ionic-angular';

//import {RouteConfig,  ROUTER_DIRECTIVES, ROUTER_PROVIDERS,
     //   LocationStrategy, HashLocationStrategy} from 'angular2/router';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [Loginservice, Mediaservice],
})

export class HomePage implements OnInit{



  private media: any = [];
   private mediaInfo: any = [];
paska:any = null;
  navOptions = {
    animate: false
  }


  public cate = "Default";
public commentOpen = false;
  public opened = false;
  public closed = true;
private uniqtags = new Set();
private tagss = [];

private param;
  categories: any;

  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController,public navParams: NavParams, public menuCtrl: MenuController, private loginservice: Loginservice, private mediaService: Mediaservice) {
      
      this.loading();
      if (localStorage.getItem("user") !== null){
              this.loginservice.setUser(JSON.parse(localStorage.getItem("user")));
              this.loginservice.logged = true;
            } else if (this.loginservice.getUser().password !== undefined){
              this.loginservice.login();
            }
      this.param = navParams.get("tagivideosivulta");

      this.categories= ['vahvistin','dinner','pöytä','cat4','cat5','cat6'];

        

      

      if (this.param) {
        this.getStuffWithTag2(this.param);
        this.cate=this.param;
      } else {
        this.getStuffWithTag2("BS");
      }
/*
      for (let i=0; i<6; i++) {
            let random = Math.floor(Math.random() * this.mediaInfo.length);
            
                    this.categories[i] = random;
                    
        }
*/

  }


navLogout() {
  this.loginservice.logout();
  this.navLogin();
}
  navVideo(id: number) {
    console.log(id);
    this.navCtrl.push(VideoPage, {firstPassed: id},  this.navOptions);
  }


sortByFavs() {
  this.media.sort(function(a, b){return b-a});
}

getStuffWithTag2(tag:string) {
        this.media = [];
        this.mediaInfo = [];

        this.mediaService.getMediaByTag(encodeURI(tag)).subscribe(
          res => {
            for (let i=res.length-1; i>=0; i--) {
                this.mediaService.getTagsByMedia(res[i].file_id).subscribe(
                      tagres => {
                          if (tagres.length > 0) {
                            if (tagres[0].tag == "BS") {
                              this.media.push(res[i]);
                              this.mediaInfo.push(this.mediaService.populateInfo(res[i].file_id, tagres));
                              
                            }
                          }
                      });
            }
          }
        );
}
 loading() {
    let loader = this.loadingCtrl.create({
      content: "",
      duration: 300
    });
    loader.present();
  }

   openMenu() {
   this.menuCtrl.open();
  }

   closeMenu() {
   this.menuCtrl.close();
 }

 toggleMenu() {
   this.menuCtrl.toggle();
 }

navHome() {
   this.navCtrl.setRoot(HomePage);
 }
 navUpload() {
   this.navCtrl.setRoot(UploadPage);
 }
  navSearch() {
   this.navCtrl.setRoot(SearchPage);
 }
  navLogin() {
   this.navCtrl.setRoot(LoginPage);
 }
 openUser(user_id: number) {
      this.navCtrl.push(ProfilePage, {firstPassed: user_id}, this.navOptions);
      console.log("asdWasp");
    }

  private addFav(fileid:number, index:number) {
      //let favjson = JSON.stringify({"file_id":fileid});

      console.log(fileid+"  index: "+index)

      this.mediaService.setFavourite(fileid).subscribe(
          data => {
            this.mediaService.getFavouritesByMediaId(fileid).subscribe(
              respo => {
                this.mediaInfo[index].likes = respo
              });
          },
          error => {
            console.log("already favorited");
          }

      );


  }
 openCat() {
    this.closed = false;
    this.opened = true;
  }
 closeCat() {
    this.closed = true;
    this.opened = false;
  }

  chooseCat(category) {
    this.cate= category
    this.opened = false;
    this.closed = true;

    this.getStuffWithTag2(category);
  }




 ngOnInit() {
    // if (!this.loginService.logged)
    //   this.router.navigate(['login']);



      
    
   
  }


}
class Info {
     public fileid: any = 0;
      public user: any = {username: null};
      public userid:any = null;
      public likes:any = [];
        public tags:any = [];
        public comments: any = [];
}


